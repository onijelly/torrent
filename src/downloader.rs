use crate::meta_info::TorrentInfo;
use priority_queue::PriorityQueue;
use bit_vec::BitVec;
use std::collections::HashMap;
use sha1::Sha1;

pub const BLOCKSIZE:u64 = 16384;
/// At any time the are at most 10 pieces in downloading map.
const MAX_NO_PIECES: usize = 10;

pub struct Downloader {
    piece_priorities: PriorityQueue<usize, usize>,
    downloading: HashMap<usize, Piece>,
}

impl Downloader {
    pub fn new(torrent_info: &TorrentInfo) -> Self {
        let mut piece_priorities = PriorityQueue::with_capacity(torrent_info.get_piece_amount());
        (0..piece_priorities.capacity()).for_each(|x| {
            piece_priorities.push(x, 0);
        });

        Self { 
            piece_priorities,
            downloading: HashMap::new(),
        }
    }

    pub fn update_priority(&mut self, bit_field: BitVec) {
        bit_field.iter().enumerate().for_each(|(pie_idx, set)| {
            let mut priority = *self.piece_priorities.get_priority(&pie_idx).unwrap_or(&0);
            priority += set as usize;
            self.piece_priorities.change_priority(&pie_idx, priority);
        });

        println!("Just updated bitfield ");
    }


    pub fn write_block(&mut self, piece_idx: usize, block_idx: usize){
        //write block to disk
        //update block field
    }
}

#[derive(Clone)]
pub struct Block{
    index: u64,
    length: u64,
    downloaded: bool,
}

impl Block{
    fn new(index: u64, length: u64) -> Self{
        Self{
            index,
            length,
            downloaded: false,
        }
    }
}

struct Piece{
   length: u64,
   offset: usize,
   hash: Sha1,
   blocks: Vec<Block>,
   is_complete: bool,
}

impl Piece {
    fn new(length: u64, offset: usize, hash: Sha1) -> Self {
        let mut blocks = Vec::new();
        let number_of_blocks = (length as f64 / BLOCKSIZE as f64).ceil() as u64;
        for i in 0..number_of_blocks {
            let block_len = if i < (number_of_blocks - 1) {
                BLOCKSIZE
            } else {
                length - (BLOCKSIZE * (number_of_blocks - 1))
            };

            blocks.push(Block::new(i, block_len));
        }

        Self{
            length,
            offset,
            hash,
            blocks,
            is_complete: false,
        }
    }

    fn get_next_block(&self) -> Option<Block> {
        self.blocks.iter().find(|x| x.downloaded == true).map(|x| x.clone())
    }
}
